﻿using System;

namespace eBisProjet.Entity
{
    public class Contrat
    {
        public string NumContrat { get; set; }
        public DateTime DateContrat { get; set; }
        public string Immatriculation { get; set; }
        public Abonnement Abonnement { get; set; }
        public Batterie Batterie { get; set; }
    }
}