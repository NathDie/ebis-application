﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson;

namespace eBisProjet.Entity
{
    public class OperationRecharge
    {
        public ObjectId _id { get; set; }
        public string Borne { get; set; }
        public string Usager { get; set; }
        public DateTime DateOperationRecharge { get; set; }

        public string Type { get; set; }
  
    }
}
