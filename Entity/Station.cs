﻿using MongoDB.Bson;

namespace eBisProjet.Entity
{
    public class Station
    {
        public string Lattitude { get; set; }
        public string Longitude { get; set; }
        public string Rue { get; set; }
        public string Ville { get; set; }
        public string CodePostal { get; set; }
       
    }

}