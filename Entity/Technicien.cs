﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson;

namespace eBisProjet.Entity
{
   public class Technicien
    {
        public ObjectId _id { get; set; }
        public string Nom { get; set; }
        public string Prenom { get; set; }
        public string ZoneGeographique { get; set; }
    }
}
