﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eBisProjet.Entity
{
   public class Incident : OperationRecharge
    {
        public TypeError IncidentError { get; set; }
        public Element Elements { get; set; }
    }
    public enum TypeError
    {
        ErreurIdentificationClient,
        ErreurBranchementVehicule,
        ErreurAccesReseau,
        ErreurConnexionAppli
    }
    public enum Element
    {
        Clavier,
        Ecran,
        CarteMere,
        Alimentation,
        Routeur,
        Firewall,
        DisqueSSD,
        DisqueSAS,

    }
}
