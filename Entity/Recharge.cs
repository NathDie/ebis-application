﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eBisProjet.Entity
{
    public class Recharge : OperationRecharge
    {
        public Batterie Batterie { get; set; }

        public DateTime DateHeureDebut { get; set; }
        public DateTime DateHeureFin { get; set; }
        public int Consommation { get; set; }
    }
}
