﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson;

namespace eBisProjet.Entity
{
    class Borne
    {
        public ObjectId _id { get; set; }
        public string Document { get; set; }
        public DateTime DateMiseService { get; set; }
        public DateTime DateDerniereRevision { get; set; }
        public Station Station { get; set; }
        public Batterie Batterie { get; set; }
    }
}
