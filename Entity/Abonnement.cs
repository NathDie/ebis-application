﻿
using System;

namespace eBisProjet.Entity
{
    public class Abonnement
    {
        public DateTime DateDebut { get; set; }
        public DateTime DateFin { get; set; }
        
    }
}