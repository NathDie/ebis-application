﻿namespace eBisProjet.Entity
{
    public class Batterie
    {
        public string Fabricant { get; set; }
        public Puissance Power { get; set; }

    }

    public enum Puissance
    {
        Normal,
        SemiRapide,
        Rapide,
    }
}