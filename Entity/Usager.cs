﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson;

namespace eBisProjet.Entity
{
    class Usager
    {
        public ObjectId _id { get; set; }
        public string Nom { get; set; }
        public string Prenom { get; set; }
        public string Adresse { get; set; }
        public string Ville { get; set; }
        public string Mail { get; set; }
        public string TelMobile { get; set; }
        public string TelFixe { get; set; }
        public List<Contrat> Contrats { get; set; }
    }
}
