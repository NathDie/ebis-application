﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using eBisProjet.Repository;
using eBisProjet.Service;
using eBisProjet.View;


namespace eBisProjet
{
    /// <summary>
    /// Logique d'interaction pour MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
           // BorneRepository borneRepository = new BorneRepository();
           // borneRepository.InsertBorne();

            //UsagerRepository usagerRepository = new UsagerRepository();
            //usagerRepository.InsertUsager();

            JournalRepository journalRepository = new JournalRepository();
            //journalRepository.InsertJournal();
           // journalRepository.InsertJournal1();
            //journalRepository.InsertJournal2();

            //TechnicienRepository technicienRepository = new TechnicienRepository();
            //technicienRepository.InsertTechnicien();

            var date1 = new DateTime(2008, 5, 1);
            Console.WriteLine(date1.ToString("dd-MM-yyyy"));
        }

        private void BornesView_Click(object sender, RoutedEventArgs e)
        {
            contentArea.Children.Clear();
            contentArea.Children.Add(new BornesView());
            
        }

        private void JournalView_Click(object sender, RoutedEventArgs e)
        {
            contentArea.Children.Clear();
            contentArea.Children.Add(new JournalView());

        }
        private void IncidentsView_Click(object sender, RoutedEventArgs e)
        {
            contentArea.Children.Clear();
            contentArea.Children.Add(new IncidentsView());
        }
        private void RechargesView_Click(object sender, RoutedEventArgs e)
        {
            contentArea.Children.Clear();
            contentArea.Children.Add(new RechargesView());
        }
        private void EntretiensView_Click(object sender, RoutedEventArgs e)
        {
            contentArea.Children.Clear();
            contentArea.Children.Add(new EntretiensView());
        }
        private void TechniciensView_Click(object sender, RoutedEventArgs e)
        {
            contentArea.Children.Clear();
            contentArea.Children.Add(new TechniciensView());
        }
    }
}
