﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using eBisProjet.Repository;
using eBisProjet.Entity;

namespace eBisProjet.View
{
    /// <summary>
    /// Logique d'interaction pour Entretiens.xaml
    /// </summary>
    public partial class EntretiensView : UserControl
    {
        public EntretiensView()
        {
            InitializeComponent();

            JournalRepository journalRepository = new JournalRepository();
            List<Entretien> entretiens = journalRepository.GetAllEntretient();
            this.DataContext = entretiens;
            listEntretiens.ItemsSource = entretiens;
            EntretientDetail.Visibility = Visibility.Hidden;
        }

        private void ListViewItem_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            var item = sender as ListViewItem;
            EntretientDetail.Visibility = Visibility.Visible;
            if (item != null)
            {
                Entretien entretien = item.Content as Entretien;
                EntretientDetail.Id.Text = entretien._id.ToString();
                EntretientDetail.DateOperationRecharge.Text = entretien.DateOperationRecharge.ToString("dd-MM-yyyy");
                EntretientDetail.Borne.Text = entretien.Borne;
                EntretientDetail.Prenom.Text = entretien.Technicien.Prenom;
                EntretientDetail.Nom.Text = entretien.Technicien.Nom;
                EntretientDetail.DetailEntretien.Text = entretien.DetailEntretien;
            }
        }
    }
}
