﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using eBisProjet.Entity;
using eBisProjet.Repository;

namespace eBisProjet.View
{
    /// <summary>
    /// Logique d'interaction pour Techniciens.xaml
    /// </summary>
    public partial class TechniciensView : UserControl
    {
        public TechniciensView()
        {
            InitializeComponent();

            TechnicienRepository technicienRepository = new TechnicienRepository();
            List<Entity.Technicien> techniciens = technicienRepository.GetAll();

            foreach (Entity.Technicien technicien in techniciens)
            {
                Console.WriteLine("XXXXXXXXXX" + technicien._id);
            }
            this.DataContext = techniciens;
            listTechniciens.ItemsSource = techniciens;

        }
    }
}
