﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using eBisProjet.Entity;
using eBisProjet.Repository;

namespace eBisProjet.View
{
    /// <summary>
    /// Logique d'interaction pour Recharges.xaml
    /// </summary>
    public partial class RechargesView : UserControl
    {
        public RechargesView()
        {
            InitializeComponent();

            JournalRepository journalRepository = new JournalRepository();
            List<Recharge> recharges = journalRepository.GetAllRecharge();
            this.DataContext = recharges;
            listRecharges.ItemsSource = recharges;
            RechargeDetail.Visibility = Visibility.Hidden;
        }

        private void ListViewItem_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            var item = sender as ListViewItem;
            RechargeDetail.Visibility = Visibility.Visible;
            if (item != null)
            {
                Recharge recharge = item.Content as Recharge;

                RechargeDetail.Id.Text = recharge._id.ToString();
                RechargeDetail.DateHeureDebut.Text = recharge.DateHeureDebut.ToString();
                RechargeDetail.DateHeureFin.Text = recharge.DateHeureFin.ToString();
                RechargeDetail.Consommation.Text = recharge.Consommation.ToString();
                RechargeDetail.DateOperationRecharge.Text = recharge.DateOperationRecharge.ToString();
                RechargeDetail.Fabriquant.Text = recharge.Batterie.Fabricant;
                RechargeDetail.Puissance.Text = recharge.Batterie.Power.ToString();
                RechargeDetail.Borne.Text = recharge.Borne.ToString();


            }
        }
    }
}
