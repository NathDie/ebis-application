﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using eBisProjet.Repository;
using eBisProjet.Entity;

namespace eBisProjet.View
{
    /// <summary>
    /// Logique d'interaction pour Bornes.xaml
    /// </summary>
    public partial class BornesView : UserControl
    {
     
        public BornesView()
        {
            InitializeComponent();
            BorneRepository borneRepository = new BorneRepository();
            List<Borne> bornes = borneRepository.GetBornes();
            
            this.DataContext = bornes;
            listBornes.ItemsSource = bornes;
            BorneDetail.Visibility = Visibility.Hidden;
        }
        private void ListViewItem_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            var item = sender as ListViewItem;
            BorneDetail.Visibility = Visibility.Visible;
            if (item != null)
            {
                Entity.Borne borne = item.Content as Entity.Borne;
                
                BorneDetail.Id.Text = borne._id.ToString();
                BorneDetail.DataMiseService.Text = borne.DateMiseService.ToString("dd-MM-yyyy");
                BorneDetail.DateDerniereRevision.Text = borne.DateDerniereRevision.ToString("dd-MM-yyyy");
                BorneDetail.Rue.Text = borne.Station.Rue;
                BorneDetail.Ville.Text = borne.Station.Ville;
                BorneDetail.CodePostal.Text = borne.Station.CodePostal;
                BorneDetail.Lattitude.Text = borne.Station.Lattitude;
                BorneDetail.Longitude.Text = borne.Station.Longitude;
                BorneDetail.Fabriquant.Text = borne.Batterie.Fabricant;
                BorneDetail.Puissance.Text = borne.Batterie.Power.ToString();

            }
        }
    }
}
