﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using eBisProjet.Entity;
using eBisProjet.Repository;

namespace eBisProjet.View
{
    /// <summary>
    /// Logique d'interaction pour Incidents.xaml
    /// </summary>
    public partial class IncidentsView : UserControl
    {
        public IncidentsView()
        {
            InitializeComponent();
            JournalRepository journalRepository = new JournalRepository();
            List<Incident> incidents = journalRepository.GetAllIncident();
            listIncidents.ItemsSource = incidents;
            IncidentDetail.Visibility = Visibility.Hidden;
        }

        private void ListViewItem_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            var item = sender as ListViewItem;
            IncidentDetail.Visibility = Visibility.Visible;
            if (item != null)
            {
                Incident incident = item.Content as Incident;

                IncidentDetail.Id.Text = incident._id.ToString();
                IncidentDetail.Borne.Text = incident.IncidentError.ToString();
                IncidentDetail.DateOperationRecharge.Text = incident.Borne;
                IncidentDetail.Incident.Text = incident.DateOperationRecharge.ToString("dd-MM-yyyy");
                IncidentDetail.Element.Text = incident.Elements.ToString();
                
         
            }
        }
    }
}
