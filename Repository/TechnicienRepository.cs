﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using eBisProjet.Entity;
using eBisProjet.Service;
using MongoDB.Bson;
using MongoDB.Bson.IO;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;
using Newtonsoft.Json;
using JsonConvert = Newtonsoft.Json.JsonConvert;

namespace eBisProjet.Repository
{
    class TechnicienRepository
    {
        public void InsertTechnicien()
        {
            ConnectDb connectDB = new ConnectDb();
            var database = connectDB.Connect();
            var technicienCollection = database.GetCollection<BsonDocument>("technicien");
            Technicien technicien = new Technicien();
            technicien._id = ObjectId.GenerateNewId();
            technicien.Prenom = "Momo";
            technicien.Nom = "Larico";
            technicien.ZoneGeographique = "Sud";

            string json = JsonConvert.SerializeObject(technicien);
            BsonDocument bson = BsonDocument.Parse(json);
            technicienCollection.InsertOne(bson);

            //var bornes = technicienCollection.InsertOne();


        }

        public List<Technicien> GetAll()
        {
            ConnectDb connectDB = new ConnectDb();
            var database = connectDB.Connect();
            var technicienCollection = database.GetCollection<BsonDocument>("technicien");

            var techniciens = technicienCollection.Find(new BsonDocument()).ToList();

            List<Technicien> ListTechniciens = new List<Technicien>();
            foreach (BsonDocument technicien in techniciens)
            {
  
                var myObjetTechnicien = BsonSerializer.Deserialize<Technicien>(technicien);
                ListTechniciens.Add(myObjetTechnicien);

            }
            return ListTechniciens;
        }

        public Technicien GetTechnicienById(string id)
        {
            ConnectDb connectDB = new ConnectDb();
            var database = connectDB.Connect();
            var borneCollection = database.GetCollection<BsonDocument>("technicien");

            var filter = Builders<BsonDocument>.Filter.Eq("_id", id);

            var technicien = borneCollection.Find(filter).FirstOrDefault();


            var oneObjectTechnicien = BsonSerializer.Deserialize<Technicien>(technicien);

            return oneObjectTechnicien;
        }

    }
}
