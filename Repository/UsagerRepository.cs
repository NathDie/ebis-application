﻿using eBisProjet.Entity;
using eBisProjet.Service;
using MongoDB.Bson;
using MongoDB.Driver;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eBisProjet.Repository
{
    class UsagerRepository
    {
        public void GetUsagers()
        {
            ConnectDb connectDB = new ConnectDb();
            var database = connectDB.Connect();
            var usagerCollection = database.GetCollection<BsonDocument>("usager");
            var usagers = usagerCollection.Find(new BsonDocument()).ToList();

            foreach (BsonDocument usager in usagers)
            {
                Console.WriteLine(usager);

            }
        }
        public void InsertUsager()
        {
            ConnectDb connectDB = new ConnectDb();
            var database = connectDB.Connect();
            var usagerCollection = database.GetCollection<BsonDocument>("usager");
           
            Abonnement abonnement1 = new Abonnement();
            abonnement1.DateDebut = new DateTime(2020, 02, 03);
            abonnement1.DateFin = new DateTime(2021, 02, 03);

            Batterie batterie1 = new Batterie();
            batterie1.Fabricant = "Sieminess";
            batterie1.Power = Puissance.Rapide;

            Contrat contrat1 = new Contrat();
            contrat1.NumContrat = "123";
            contrat1.DateContrat = new DateTime(2020, 01, 02);
            contrat1.Immatriculation = "AA-201-AB";

            contrat1.Abonnement = abonnement1;
            contrat1.Batterie = batterie1;

            List<Contrat> contrats = new List<Contrat>();
            contrats.Add(contrat1);

            Usager usager1 = new Usager();
            usager1._id = ObjectId.GenerateNewId();
            usager1.Nom = "toto";
            usager1.Prenom = "tata";
            usager1.Ville = "Lille";
            usager1.Mail = "toto.tata@gmail.com";
            usager1.Adresse = "30 rue de Lille";
            usager1.TelFixe = "0325451245";
            usager1.TelMobile = "0667845298";
            usager1.Contrats = contrats;

            string json = JsonConvert.SerializeObject(usager1);
            BsonDocument bson = BsonDocument.Parse(json);
            usagerCollection.InsertOne(bson);

        }
    }
}
