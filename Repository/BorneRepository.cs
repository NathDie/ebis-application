﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using eBisProjet.Entity;
using eBisProjet.Service;
using MongoDB.Bson;
using MongoDB.Bson.IO;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;
using Newtonsoft.Json;
using JsonConvert = Newtonsoft.Json.JsonConvert;

namespace eBisProjet.Repository
{
    class BorneRepository
    {
        public List<Borne> GetBornes()
        {
            ConnectDb connectDB = new ConnectDb();
            var database = connectDB.Connect();
            var borneCollection = database.GetCollection<BsonDocument>("borne");
            var bornes = borneCollection.Find(new BsonDocument()).ToList();
            List<Borne> ListBornes = new List<Borne>();
            foreach (BsonDocument borne in bornes)
            {
                Console.WriteLine(borne);
                var myObjetBorne = BsonSerializer.Deserialize<Borne>(borne);
                ListBornes.Add(myObjetBorne);
                
            }
            return ListBornes;
        }

        public void InsertBorne()
        {
            ConnectDb connectDB = new ConnectDb();
            var database = connectDB.Connect();
            var borneCollection = database.GetCollection<BsonDocument>("borne");
            Borne borne1 = new Borne();
            borne1._id = ObjectId.GenerateNewId();
            borne1.Document = "Borne";
            borne1.DateDerniereRevision = new DateTime(2011,06,05);
            borne1.DateMiseService = new DateTime(2001, 04, 12);

            Station station1 = new Station();
            station1.Lattitude = "323.456";
            station1.Longitude = "-124.5879";
            station1.Rue = " Lille";
            station1.CodePostal = "59000";
            station1.Ville = "Lille";

            borne1.Station = station1;

            Batterie batterie1 = new Batterie();
            batterie1.Fabricant = "Sieminess";
            batterie1.Power = Puissance.Rapide;

            borne1.Batterie = batterie1;

            string json = JsonConvert.SerializeObject(borne1);
            BsonDocument bson = BsonDocument.Parse(json);
            borneCollection.InsertOne(bson);

        }

        public Borne GetBorneById(string id)
        {
            ConnectDb connectDB = new ConnectDb();
            var database = connectDB.Connect();
            var borneCollection = database.GetCollection<BsonDocument>("borne");
            
            var filter = Builders<BsonDocument>.Filter.Eq("_id", id);

            var borne = borneCollection.Find(filter).FirstOrDefault();

         
            var oneObjectBorne = BsonSerializer.Deserialize<Borne>(borne);

            //Console.WriteLine(oneObjectBorne);
            return oneObjectBorne;
        }
    }
}
