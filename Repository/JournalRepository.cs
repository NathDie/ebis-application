﻿using eBisProjet.Entity;
using eBisProjet.Service;
using MongoDB.Bson;
using MongoDB.Bson.IO;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;
using Newtonsoft.Json;
using JsonConvert = Newtonsoft.Json.JsonConvert;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eBisProjet.Repository
{
    class JournalRepository
    {
        public void InsertJournal()
        {
            //entretien
            ConnectDb connectDB = new ConnectDb();
            var database = connectDB.Connect();
            var journalCollection = database.GetCollection<BsonDocument>("journal");

            Technicien technicien1 = new Technicien();
            technicien1._id = ObjectId.Parse("60881da6933e247bdcd413a5");
            technicien1.Nom = "Larico";
            technicien1.Prenom = "Momo";
            technicien1.ZoneGeographique = "Sud";

            Entretien entretien1 = new Entretien();
            entretien1.DetailEntretien = "Effectuer";
            entretien1.DateOperationRecharge = DateTime.Now;
            entretien1.Borne = "6088672d5f68458455b8971d";
            entretien1._id = ObjectId.GenerateNewId();
            entretien1.Technicien = technicien1;
            entretien1.Type = "entretien";
            entretien1.Usager = "60886e051d35cd7aa7c46eea";

            string json = JsonConvert.SerializeObject(entretien1);
            BsonDocument bson = BsonDocument.Parse(json);
            journalCollection.InsertOne(bson);
        }
        // obsolete
        public void InsertJournal1()
        {
            //recharge
            ConnectDb connectDB = new ConnectDb();
            var database = connectDB.Connect();
            var journalCollection = database.GetCollection<BsonDocument>("journal");

            Recharge recharge1 = new Recharge();

            Batterie batterie1 = new Batterie();
            batterie1.Fabricant = "Bosch";
            batterie1.Power = Puissance.SemiRapide;

            recharge1._id = ObjectId.GenerateNewId();
            recharge1.Batterie = batterie1;
            recharge1.DateHeureDebut = new DateTime(2020, 02, 01);
            recharge1.DateHeureFin = new DateTime(2020, 02, 03);
            recharge1.Consommation = 25;
            recharge1.Borne = "60886e041d35cd7aa7c46ee9";
            recharge1.Type = "recharge";
            recharge1.Usager = "60886e051d35cd7aa7c46eea";
            recharge1.DateOperationRecharge = DateTime.Now;

            string json1 = JsonConvert.SerializeObject(recharge1);
            BsonDocument bson1 = BsonDocument.Parse(json1);
            journalCollection.InsertOne(bson1);
        }
        public void InsertJournal2()
        {
            ConnectDb connectDB = new ConnectDb();
            var database = connectDB.Connect();
            var journalCollection = database.GetCollection<BsonDocument>("journal");
            
            //Incident
            Incident incident1 = new Incident();
            incident1.Type = "incident";
            incident1._id = ObjectId.GenerateNewId();
            incident1.DateOperationRecharge = DateTime.Now;
            incident1.Borne = "60886e041d35cd7aa7c46ee9";
            incident1.Usager = "60886e051d35cd7aa7c46eea";
            incident1.Elements = Element.Clavier;
            incident1.IncidentError = TypeError.ErreurIdentificationClient;

            string json2 = JsonConvert.SerializeObject(incident1);
            BsonDocument bson2 = BsonDocument.Parse(json2);
            journalCollection.InsertOne(bson2);
        }

        [Obsolete]
        public List<Incident> GetAllIncident()
        {
            ConnectDb connectDB = new ConnectDb();
            var database = connectDB.Connect();
            var journalCollection = database.GetCollection<BsonDocument>("journal");

            var filter = Builders<BsonDocument>.Filter.Eq("Type", "incident");

            var incidents = journalCollection.Find(filter).ToList<BsonDocument>();

            List<Incident> ListJournalIncident = new List<Incident>();
            foreach (var bson in incidents)
            {
                var myObjetIncident = BsonSerializer.Deserialize<Incident>(bson);
                ListJournalIncident.Add(myObjetIncident);

            }
            return ListJournalIncident;
        }

        [Obsolete]
        public List<Recharge> GetAllRecharge()
        {
            ConnectDb connectDB = new ConnectDb();
            var database = connectDB.Connect();
            var journalCollection = database.GetCollection<BsonDocument>("journal");

            var filter = Builders<BsonDocument>.Filter.Eq("Type", "recharge");

            var recharges = journalCollection.Find(filter).ToList<BsonDocument>();

            List<Recharge> ListJournalRecharge = new List<Recharge>();
            foreach (var bson in recharges)
            {
                
                var myObjetRecharge = BsonSerializer.Deserialize<Recharge>(bson);
                ListJournalRecharge.Add(myObjetRecharge);

            }
            return ListJournalRecharge;
        }

        [Obsolete]
        public List<Entretien> GetAllEntretient()
        {
            ConnectDb connectDB = new ConnectDb();
            var database = connectDB.Connect();
            var journalCollection = database.GetCollection<BsonDocument>("journal");

            //var filter = Builders<BsonDocument>.Filter.Eq("Type", "entretien");
            //var entretiens = journalCollection.Find(filter);
            //Console.WriteLine("les ENTRETIENTS  :  " +  entretiens);

            
            var filter = Builders<BsonDocument>.Filter.Eq("Type", "entretien");
            List<Entretien> ListEntretients = new List<Entretien>();
            var entretients = journalCollection.Find(filter).ToList<BsonDocument>();
            foreach(BsonDocument bson in entretients)
            {
                var myObjetEntretien = BsonSerializer.Deserialize<Entretien>(bson);
                ListEntretients.Add(myObjetEntretien);
            }
            return ListEntretients;
        }
    }
}
